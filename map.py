"""Test on making the google map appear"""
# [Start import]
import logging
import cloudstorage as gcs
import webapp2
import os
import cgi
from google.appengine.api import app_identity

# [Display the inital Google Map and coordinate input form when user first access the page]
# TODO: Use a template instead of the write
class MapSetup(webapp2.RequestHandler):
    # [Get Method that display the html]
    def get(self):
        
        # Test for writing the dummy.json file to GCS
        # bucket_name = os.environ.get('BUCKET_NAME',
        #                          app_identity.get_default_gcs_bucket_name())

        # self.response.headers['Content-Type'] = 'text/plain'
        # self.response.write('Demo GCS Application running from Version: '
        #                     + os.environ['CURRENT_VERSION_ID'] + '\n')
        # self.response.write('Using bucket name: ' + bucket_name + '\n\n')
        # #[END get_default_bucket]
    
        # bucket = '/' + bucket_name
        # try:
        #   self.getjson(bucket)

        # Test code to display the website using template.
        # path = os.path.join(os.path.dirname(__file__), 'map.html')
        # self.response.out.write(template.render(path))
        
        # output the website code for display
        self.response.write("""
          <html>
            <head>
                <link type="text/css" rel="stylesheet" href="/stylesheet/main.css" />
            </head>
            <body>
                <div id="map"></div>
              <form name="coordinate_search" action="/out" method="post">
                <div>Latitude: <input type="number" name="lat"></div>
                <div>Longitude: <input type="number" name="lon"></div>
                <div><input type="submit" value="Search"></div>
              </form>
               <script>
             // Create a script tag and set the USGS URL as the source.
                var map;
            // initialize the map, set the center of the map upon loading, and set the type of map to display.
              function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 3,
                  center: {lat: 42.361145, lng: -71.057083},
                  mapTypeId: 'terrain'
                });

                // Create a <script> tag and set the USGS URL as the source.
                var script = document.createElement('script');

                // This example uses a local copy of the GeoJSON stored at
                // http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojsonp
                //script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
                script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
                document.getElementsByTagName('head')[0].appendChild(script);

                map.data.setStyle(function(feature) {
                  var magnitude = feature.getProperty('mag');
                  return {
                    icon: getCircle(magnitude)
                  };
                });
              }

              function getCircle(magnitude) {
                return {
                  path: google.maps.SymbolPath.CIRCLE,
                  fillColor: 'yellow',
                  fillOpacity: .3,
                  scale: Math.pow(2, magnitude) / 2,
                  strokeColor: 'white',
                  strokeWeight: 1
                };
              }

              function eqfeed_callback(results) {
                map.data.addGeoJson(results);
              }
            </script>
              <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4Npm_Vh1noInop9bB4Lp0rBEJIIBvbJk&callback=initMap">
    </script>
            </body>
          </html>""")
    
    # Read the input file (dump.json), then create a file in GCS that copies the information, then close the file when finish.
    def getjson(self, bucket):
        filename=bucket +'/dump.json'
        self.response.write('read json file %s\n' % filename)
        gcs_file = gcs.open(filename)
        #self.response.write(gcs_file.readline())
        gcs_file.seek(-1024, os.SEEK_END)
        #self.response.write(gcs_file.read())
        gcs_file.close()
    
    # POST method that will display the map using user inputted coordinates as the center.
    # Doesn't function atm.
#TODO: Use template instead of writing out the script.
class output(webapp2.RequestHandler):
    def post(self):
        # self.response.out.write('<html><body>You wrote:<pre>')
        lat = float(cgi.escape(self.request.get('lat')))
        long = float(cgi.escape(self.request.get('lat')))

        # self.response.out.write(cgi.escape(self.request.get('lat')))
        # self.response.out.write(cgi.escape(self.request.get('long')))
        # self.response.out.write('</pre></body></html>')
        self.response.write("""
          <html>
            <head>
                <link type="text/css" rel="stylesheet" href="/stylesheet/main.css" />
            </head>
            <body>
                <div id="menu"></div>

                <div id="header">
                <form name="coordinate_search" action="/out" method="post">
                <div>Latitude: <input type="number" name="lat"></div>
                <div>Longitude: <input type="number" name="lon"></div>
                <div><input type="submit" value="Search"></div>
              </form>
              <img id="img" src="logo white.png" style="width: 400px; height: 50px; padding-left: 700px;"></img>
            </div>


            <div id="info_frame">
                <!--<div><p>You entered<br/>Latitude 43, Longitude -71.<br/>-->
                    Your water scarcity risk is
                <span>LOW</span>.</p>
            <!--</div>-->
            </div>

            <div id="map_frame">
                <div id="map"></div>
            </div>

               <script>
        // Create a script tag and set the USGS URL as the source.
      var map;
      // initialize the map, set the center of the map upon loading, and set the type of map to display.
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: {lat: %f, lng: %f},
          mapTypeId: 'terrain'
        });

        // Create a <script> tag and set the USGS URL as the source.
        var script = document.createElement('script');

        // This example uses a local copy of the GeoJSON stored at
        // http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.geojsonp
        //script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
        script.src = 'https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js';
        document.getElementsByTagName('head')[0].appendChild(script);

        map.data.setStyle(function(feature) {
          var magnitude = feature.getProperty('risk');
          return {
            icon: getCircle(magnitude)
          };
        });
      }

      function getCircle(magnitude) {
        return {
          path: google.maps.SymbolPath.CIRCLE,
          fillColor: 'yellow',
          fillOpacity: .3,
          scale: Math.pow(2, magnitude) / 2,
          strokeColor: 'white',
          strokeWeight: 1
        };
      }

      function eqfeed_callback(results) {
        map.data.addGeoJson(results);
      }
    </script>
              <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4Npm_Vh1noInop9bB4Lp0rBEJIIBvbJk&callback=initMap">
    </script>
            </body>
          </html>"""% (lat, long))

app = webapp2.WSGIApplication([
    ('/', MapSetup),
    ('/out', output),
], debug=True)
