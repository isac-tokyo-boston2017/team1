import numpy as np
import sklearn
from sklearn import tree
from sklearn.model_selection import train_test_split
from IPython.display import Image
import pydotplus

from NDFiles import grace_files, gpm_files
from tablize_DATA import tablize_DATA

metric_thresh = 2

data_table = tablize_DATA(gpm_files=gpm_files,grace_files=grace_files,save=False,savefile='output_table.csv')

# Risk Computation
metric = np.log(1 / (data_table[:,-1] * data_table[:,-2]))
risk = np.ma.filled(metric > metric_thresh,0).astype('int')

# Test
training_data, testing_data, training_label, testing_label = train_test_split(data_table, risk, test_size=0.8)
clf = tree.DecisionTreeClassifier()
clf.fit(training_data,training_label)

res = clf.predict(testing_data)
score = np.sum(np.equal(res,testing_label))/float(testing_label.size)

dot_data =  tree.export_graphviz(clf, out_file=None,feature_names=(\
    "sin of month","lat","lon","groundwater thickness","rainfall"),\
                                 class_names="water shortage risk",\
                                 filled=True, rounded=True,
                                 special_characters=True)
graph = pydotplus.graph_from_dot_data(dot_data)
Image(graph.create_png())


print(score)