import numpy as np
from NASADataHandler import DataHandler

def tablize_DATA(gpm_files,grace_files,save=False,savefile=None):
    handler = DataHandler()
    handler.load_GPM(gpm_files)
    handler.load_GRACE(grace_files)
    handler.reduce_spatial(latb=(40,44),lonb=(-73,-69))

    GPM_time = np.vstack((handler.GPM_year, handler.GPM_month))
    GRACE_time = np.vstack((handler.GRACE_year, handler.GRACE_month))

    grid = []
    gpid = []
    for gpt in range(GPM_time.shape[1]):
        for grt in range(GRACE_time.shape[1]):
            same = np.alltrue(GPM_time[:,gpt] == GRACE_time[:,grt])
            if np.alltrue(GPM_time[:,gpt] == GRACE_time[:,grt]):
                grid.append(grt)
                gpid.append(gpt)
    inds = np.vstack((np.asarray(grid),np.asarray(gpid)))

    groundwater = handler.lwe_thickness[inds[0,:]]
    gcomp = np.ma.compressed(groundwater)
    gmean = np.mean(gcomp[np.logical_and(gcomp < 30000,gcomp > 0)])
    groundwater[np.logical_or(groundwater > 30000,groundwater < 0)] = gmean
    rainfall = handler.precipitation[inds[1,:]]

    gr_month = np.asarray(handler.GRACE_month)
    gr_year = np.asarray(handler.GRACE_year)
    gp_month = np.asarray(handler.GPM_month)
    gp_year = np.asarray(handler.GPM_year)

    lat = handler.latitude
    lon = handler.longitude

    # Handler should clean up here

    xx,yy,zz = np.meshgrid(np.arange(rainfall.shape[2]),\
                           np.arange(rainfall.shape[1]),\
                           np.arange(rainfall.shape[0]))
    table = np.vstack((zz.ravel(),xx.ravel(),yy.ravel(),groundwater.ravel(),rainfall.ravel())).T
    # table[:,1] indexes longitude
    # table[:,2] indexes latitude
    # table[:,3] indexes month

    table[:, 0] = np.sin((gp_month[table[:,0].astype('int')]-1)*(np.pi/12))
    table[:, 1] = lon[table[:,1].astype('int')]
    table[:, 2] = lat[table[:,2].astype('int')]

    if save:
        print("Saving data to csv...")
        n = 0
        if savefile is None:
            savefile = 'output_table.csv'
        with open(savefile, 'wb') as csvfile:
            csvfile.write('#time_sin,longitude,latitude,groundwater,rainfall\n')
            for row in table:
                csvfile.write('%f,%f,%f,%f,%f\n' % (row[0],row[1],row[2],row[3],row[4]))
                n = n + 1
                if n > 20000:
                    break
                print(n)

    return table
"""
xx,yy,zz = np.meshgrid(np.arange(groundwater.shape[2]),\
                       np.arange(groundwater.shape[1]),\
                       np.arange(groundwater.shape[0]))
table = np.vstack((groundwater.ravel(), xx.ravel(), yy.ravel(), zz.ravel(), zz.ravel())).T
# table[:,1] indexes longitude
# table[:,2] indexes latitude
# table[:,3] indexes month

table[:, 1] = lon[table[:, 1].astype('int')]
table[:, 2] = lat[table[:, 2].astype('int')]
table[:, 3] = gr_month[table[:, 3].astype('int')]
table[:, 4] = gr_year[table[:, 4].astype('int')]

print("Saving GRACE data to csv...")

outfile = 'grace_table.csv'
with open(outfile, 'ab') as csvfile:
    n = 0
    for row in table:
        csvfile.write('%f,%f,%f,%d\n' % (row[0],row[1],row[2],row[3]))
        n = n+1
        print(n)
"""