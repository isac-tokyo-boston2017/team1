import numpy as np

# NetCDF4 Tools
from netCDF4 import Dataset

# Plotting Tools
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

#from NASADataHandler import GRACE_DATA, GPM_DATA

# Dataset Filenames
#grace_files = ("data_GRACE\GRCTellus.CSR.200204_201607.LND.RL05.DSTvSCS1409.nc",)#\
#             "data_GRACE\GRCTellus.GFZ.200204_201607.LND.RL05.DSTvSCS1409.nc",\
#             "data_GRACE\GRCTellus.JPL.200204_201607.LND.RL05_1.DSTvSCS1411.nc")
data_GRACE = GRACE_DATA(grace_files)

"""
file_obj = Dataset("data_GRACE\GRCTellus.grc.200204_201607.LND.RL05.DSTvSCS1409.nc", "r", format="NETCDF4")
data_GRACE_LWE = file_obj.variables['lwe_thickness'][:]
file_obj.close()
"""

# Convert LAT/LON to 2-D Grid
grc_lon_0 = data_GRACE.lon.mean()
grc_lat_0 = data_GRACE.lat.mean()
grc_lon, grc_lat = np.meshgrid(data_GRACE.lon, data_GRACE.lat)

#grc_map = Basemap(width=5000000,height=3500000,resolution='l',projection='stere',lat_ts=40,lat_0=grc_lat_0,lon_0=grc_lon_0)
grc_map = Basemap(resolution='l',projection='merc',lat_ts=40,lat_0=grc_lat_0,lon_0=grc_lon_0)
grc_xi, grc_yi = grc_map(grc_lon, grc_lat)

# Plot Data
cs = grc_map.pcolor(grc_xi,grc_yi,np.squeeze(data_GRACE.lwe[0]))

# Add Grid Lines
grc_map.drawparallels(np.arange(-80., 81., 10.), labels=[1,0,0,0], fontsize=10)
grc_map.drawmeridians(np.arange(-180., 181., 10.), labels=[0,0,0,1], fontsize=10)

# Add Coastlines, States, and Country Boundaries
grc_map.drawcoastlines()
grc_map.drawstates()
grc_map.drawcountries()

# Add Colorbar
cbar = grc_map.colorbar(cs, location='bottom', pad="10%")
#cbar.set_label(data_GRACE.unit)

# Add Title
plt.title('GRC Data 0')

plt.show()
plt.close()
plt.ion()

print("Process GPM...")
"""
gpm_files = ("data_GPM\\3B-MO.MS.MRG.3IMERG.20140312-S000000-E235959.03.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20140401-S000000-E235959.04.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20140501-S000000-E235959.05.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20140601-S000000-E235959.06.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20140701-S000000-E235959.07.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20140801-S000000-E235959.08.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20140901-S000000-E235959.09.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20141001-S000000-E235959.10.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20141101-S000000-E235959.11.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20141201-S000000-E235959.12.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150101-S000000-E235959.01.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150201-S000000-E235959.02.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150301-S000000-E235959.03.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150401-S000000-E235959.04.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150501-S000000-E235959.05.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150601-S000000-E235959.06.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150701-S000000-E235959.07.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150801-S000000-E235959.08.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20150901-S000000-E235959.09.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20151001-S000000-E235959.10.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20151101-S000000-E235959.11.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20151201-S000000-E235959.12.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160101-S000000-E235959.01.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160201-S000000-E235959.02.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160301-S000000-E235959.03.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160401-S000000-E235959.04.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160501-S000000-E235959.05.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160601-S000000-E235959.06.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160701-S000000-E235959.07.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160801-S000000-E235959.08.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20160901-S000000-E235959.09.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20161001-S000000-E235959.10.V04A.HDF5.dap.nc4",
             "data_GPM\\3B-MO.MS.MRG.3IMERG.20161101-S000000-E235959.11.V04A.HDF5.dap.nc4")
"""
data_GPM = GPM_DATA(gpm_files)

file_obj = Dataset("data_GPM\\3B-MO.MS.MRG.3IMERG.20140312-S000000-E235959.03.V04A.HDF5.dap.nc4", "r", format="NETCDF4")
data_GPM.pcp = np.expand_dims(np.transpose(file_obj.variables['precipitation'][:]),axis=0)
file_obj.close()


print("GPM Data Loaded")

# Convert LAT/LON to 2-D Grid
gpm_lon_0 = data_GPM.lon.mean()
gpm_lat_0 = data_GPM.lat.mean()
gpm_lon, gpm_lat = np.meshgrid(data_GPM.lon, data_GPM.lat)

gpm_map = Basemap(resolution='l',projection='merc',lat_ts=40,lat_0=gpm_lat_0,lon_0=gpm_lon_0)
gpm_xi, gpm_yi = gpm_map(gpm_lon, gpm_lat)

print("Plotting GPM Data")

# Plot Data
cs = gpm_map.pcolor(gpm_xi,gpm_yi,np.squeeze(data_GPM.pcp[0]))

# Add Grid Lines
gpm_map.drawparallels(np.arange(-80., 81., 10.), labels=[1,0,0,0], fontsize=10)
gpm_map.drawmeridians(np.arange(-180., 181., 10.), labels=[0,0,0,1], fontsize=10)

# Add Coastlines, States, and Country Boundaries
gpm_map.drawcoastlines()
gpm_map.drawstates()
gpm_map.drawcountries()

# Add Colorbar
cbar = gpm_map.colorbar(cs, location='bottom', pad="10%")
cbar.set_label(data_GRACE.unit)

# Add Title
plt.title('GPM Data 0')

plt.show()

"""
"""
print()