import numpy as np

import datetime

# NetCDF4 Tools
from netCDF4 import Dataset

"""
Restrictions: LAT and LONG MUST be uniformly spaced in inputs
"""

class DataHandler():
    def __init__(self):
        self.GRACE_loaded = False
        self.GPM_loaded = False

        self.latitude = None
        self.longitude = None

        # GRACE
        self.lwe_thickness=None
        # GPM
        self.precipitation=None

        # Epoch Date
        self.GRACE_epoch = datetime.date(2002,1,1)

    def update_latitude(self,latitude):
        # Force to be -90 to 90
        if latitude[0] >= 0:
            latitude = latitude - 90

        # If this is a new space
        if self.latitude is None:
            self.latitude = latitude
            return 1

        # New data is longer
        if len(latitude) > len(self.latitude):
            factor = len(latitude) / len(self.latitude)
            self.latitude = np.repeat(self.latitude,factor)
            if self.lwe_thickness is not None:
                self.lwe_thickness = np.repeat(self.lwe_thickness,factor, axis=1)
            if self.precipitation is not None:
                self.precipitation = np.repeat(self.precipitation,factor, axis=1)
            return 1

        if (len(self.latitude) > len(latitude)):
            factor = len(self.latitude) / len(latitude)
            return factor

    def update_longitude(self,longitude):
        # Force to be -180 to 180
        if longitude[0] >= 0:
            longitude = longitude - 180

        # If this is a new space
        if self.longitude is None:
            self.longitude = longitude
            return 1

        # New data is longer
        if len(longitude) > len(self.longitude):
            factor = len(longitude) / len(self.longitude)
            self.longitude = np.repeat(self.longitude,factor)
            if self.lwe_thickness is not None:
                self.lwe_thickness = np.repeat(self.lwe_thickness,factor, axis=2)
            if self.precipitation is not None:
                self.precipitation = np.repeat(self.precipitation,factor, axis=2)
            return 1

        if (len(self.longitude) > len(longitude)):
            factor = len(self.longitude) / len(longitude)
            return factor

    def reduce_spatial(self, latb=(-90,90), lonb=(-180,180)):
        lat_ind = np.logical_and(self.latitude > latb[0],self.latitude > latb[1])
        lon_ind = np.logical_and(self.longitude > lonb[0],self.longitude > lonb[1])
        self.latitude = self.latitude[lat_ind]
        self.longitude = self.longitude[lon_ind]
        if self.lwe_thickness is not None:
            self.lwe_thickness = self.lwe_thickness[:,lat_ind,:]
            self.lwe_thickness = self.lwe_thickness[:,:,lon_ind]
        if self.precipitation is not None:
            self.precipitation = self.precipitation[:,lat_ind,:]
            self.precipitation = self.precipitation[:,:,lon_ind]

    def load_GRACE(self, datafiles):
        lwe_thickness = []
        for fn in datafiles:
            file_obj = Dataset(fn, "r", format="NETCDF4")
            lat = file_obj.variables['lat'][:]
            lon = file_obj.variables['lon'][:]
            self.time = file_obj.variables['time'][:]
            lwe = file_obj.variables['lwe_thickness'][:]

            la_factor = self.update_latitude(lat)
            #if factor > 1:
            #    lwe = np.repeat(lwe, factor, axis=1)
            #print(la_factor)

            lo_factor = self.update_longitude(lon)
            #if factor > 1:
            #    lwe = np.repeat(lwe, factor, axis=2)
            #print(lo_factor)

            lwe_thickness.append(np.expand_dims(lwe,axis=0))
            file_obj.close()
            print("Loaded {0}").format(fn)

        #self.tim = np.squeeze(np.mean(tim, axis=0))
        self.lwe_thickness = np.squeeze(np.ma.mean(lwe_thickness, axis=0))
        self.lwe_thickness = np.repeat(np.repeat(self.lwe_thickness,la_factor,axis=1),lo_factor,axis=2)
        self.GRACE_loaded = True

        self.GRACE_month = []
        self.GRACE_year = []
        for days in self.time:
            self.GRACE_month.append((self.GRACE_epoch + datetime.timedelta(days=days)).month)
            self.GRACE_year.append((self.GRACE_epoch + datetime.timedelta(days=days)).year)


    def load_GPM(self, datafiles, time_start=(2014,03,01)): # Default GPM date is 2014-04-01
        precipitation = []
        self.GPM_month = []
        self.GPM_year = []
        mon = time_start[1]-1
        for fn in datafiles:
            file_obj = Dataset(fn, "r", format="NETCDF4")
            lat = file_obj.variables['lat'][:]
            lon = file_obj.variables['lon'][:]
            pcp = np.transpose(file_obj.variables['precipitation'][:])

            factor = self.update_latitude(lat)
            if factor > 1:
                pcp = np.repeat(pcp,factor,axis=0)
            factor = self.update_longitude(lon)
            if factor > 1:
                pcp = np.repeat(pcp,factor,axis=1)

            precipitation.append(np.expand_dims(pcp,axis=0))
            file_obj.close()
            print("Loaded {0}").format(fn)


            self.GPM_month.append(np.mod(mon,12)+1)
            self.GPM_year.append(mon/12 + time_start[0])
            mon = mon+1

        self.precipitation = np.ma.concatenate(precipitation,axis=0)
        self.GPM_loaded = True
